/*
Документация по работе в шаблоне:
Документация слайдера: https://swiperjs.com/
Сниппет(HTML): swiper
*/

import Swiper, {Autoplay, Navigation, Pagination, Thumbs, Grid} from 'swiper';
Swiper.use([Autoplay, Navigation, Pagination, Thumbs, Grid]);

// Инициализация слайдеров
function initSliders() {
    // Перечень слайдеров
    if (document.querySelector('.promo-slider')) {

        new Swiper('.promo-slider', {
            loop: true,
            slidesPerView: 1,
            spaceBetween: 0,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            autoplay: {
                delay: 5000,
            },
        });
    }

    if (document.querySelector('.rubric-slider')) {

        new Swiper('.rubric-slider', {
            loop: false,
            slidesPerView: 'auto',
            spaceBetween: 18,
            breakpoints: {
                "768": {
                    spaceBetween: 24,
                },
            },
        });
    }

    if (document.querySelector('.unit-brands-slider')) {

        let unitBrands = new Swiper('.unit-brands-slider', {
            loop: true,
            slidesPerView: 'auto',
            spaceBetween: 0,
            breakpoints: {
                "1200": {
                    slidesPerView: 4,
                    grid: {
                        rows: 2,
                    },
                },
            },
            navigation: {
                nextEl: "[data-brands-prev]",
                prevEl: "[data-brands-next]",
            },
        });
    }

    if (document.querySelector('.unit-line-slider')) {

        let unitLineSlider = new Swiper('.unit-line-slider', {
            slidesPerView: 'auto',
            spaceBetween: 0,
            navigation: {
                nextEl: '.unit-button-next',
                prevEl: '.unit-button-prev',
            },
        });

        document.addEventListener('click', sliderNav);
        function sliderNav(event) {
            if (event.target.closest('[data-unit-prev]')) {
                let unit = event.target.closest('[data-unit]');
                let unitPrev = unit.querySelector('.unit-button-prev');
                unitPrev.click();
            }
            if (event.target.closest('[data-unit-next]')) {
                let unit = event.target.closest('[data-unit]');
                let unitNext = unit.querySelector('.unit-button-next');
                unitNext.click();
            }
        }
    }

    if (document.querySelector('[data-receipts]')) {

        new Swiper('[data-receipts]', {
            loop: false,
            slidesPerView: 'auto',
            spaceBetween: 0,
            navigation: {
                prevEl: '[data-receipts-prev]',
                nextEl: '[data-receipts-next]',
            },
        });
    }

    // Product nav
    function productNav() {

        if (document.querySelector('[data-product-nav]')) {

            let productNavSlider = new Swiper('[data-product-nav]', {
                init: false,
                loop: false,
                slidesPerView: 'auto',
                spaceBetween: 0,
            });

            if (document.documentElement.clientWidth < 768) {
                productNavSlider.init();
            }

            window.addEventListener('resize', function(event) {
                let isNavSlider = document.querySelector('[data-product-nav]').classList.contains('swiper-initialized');
                let winWidth = document.documentElement.clientWidth;
                if (winWidth < 768) {
                    if(!isNavSlider) {
                        productNavSlider.init();
                    }
                }
                else  {
                    if(isNavSlider) {
                        productNavSlider.destroy();
                    }
                }
            }, true);

        }
    }
    productNav();

    // Collections
    if (document.querySelector('[data-collection-slider]')) {
        let collectionSlider = new Swiper('[data-collection-slider]', {
            slidesPerView: 'auto',
            spaceBetween: 0,
            navigation: {
                nextEl: '[data-slider-next]',
                prevEl: '[data-slider-prev]',
            },
        });

        document.addEventListener('click', collectionNav);
        function collectionNav(event) {
            if (event.target.closest('[data-collection-prev]')) {
                event.target.closest('[data-collection]').querySelector('[data-slider-prev]').click();
            }
            if (event.target.closest('[data-collection-next]')) {
                event.target.closest('[data-collection]').querySelector('[data-slider-next]').click();
            }
        }
    }

}

window.addEventListener("load", function (e) {
    // Запуск инициализации слайдеров
    initSliders();
});
