"use strict"

import * as flsFunctions from "./components/functions.js"; // Полезные функции
import maskPhone from './forms/phone-mask.js'; // Маска ввода для форм
import collapse from './components/collapse.js'; // Сворачиваемые блоки
import {Fancybox} from "@fancyapps/ui"; // Fancybox modal gallery
import SimpleBar from 'simplebar'; // Кастомный скролл
import Spoilers from "./components/spoilers.js";
import Dropdown from "./components/dropdown.js";
import {WebpMachine} from "webp-hero"
// Sliders
import "./components/sliders.js";
import Swiper from "swiper";

// Проверка поддержки webp
flsFunctions.isWebp();

// Добавление класса после загрузки страницы
flsFunctions.addLoadedClass();

// IE Webp Support
const webpMachine = new WebpMachine();
webpMachine.polyfillDocument();

// Маска для ввода номера телефона
maskPhone('input[name="phone"]');

// Modal Fancybox
Fancybox.bind('[data-fancybox]', {
    autoFocus: false,
    zoom: false,
});

// Сворачиваемые блоки
collapse();


// Spoilers
Spoilers();

// Dropdown
Dropdown();

// Custom Scroll
// Добавляем к блоку атрибут data-simplebar

if (document.querySelectorAll('[data-simplebar]').length) {
    document.querySelectorAll('[data-simplebar]').forEach(scrollBlock => {
        new SimpleBar(scrollBlock, {
            autoHide: true
        });
    });
}

// Favorite button
document.addEventListener('click', favorite);
function favorite(event) {
    if (event.target.closest('[data-item-favorite]')) {
        let isAdded = event.target.closest('[data-item-favorite]').classList.contains('added');
        if(!isAdded) {
            Fancybox.show([{ src: "#add-favorite", type: "inline" }]);
        }
        event.target.closest('[data-item-favorite]').classList.toggle('added');
        const item = event.target.closest('[data-item]');
        item.classList.toggle('favorite-added');
    }
}

// Compare button
document.addEventListener('click', compare);
function compare(event) {
    if (event.target.closest('[data-item-compare]')){
        event.target.closest('[data-item-compare]').classList.toggle('added');
    }
}

// Navigation
document.addEventListener('click', navToggle);
function navToggle(event) {
    if (event.target.closest('[data-nav-toggle]')) {
        document.querySelector('.nav').classList.toggle('open');
        document.querySelector('.tapbar [data-nav-toggle]').classList.toggle('active');
        document.querySelector('body').classList.toggle('overflow-hidden');
        document.querySelector('.nav-secondary').classList.remove('nav-secondary--open');
    }
    return false
}

function navigation() {
    const nav = document.querySelector('.nav');
    const navSecondary = document.querySelector('.nav-secondary');
    const navSecondarySections = document.querySelectorAll('[data-nav-target]');
    const navSecondaryBackward = document.querySelector('[data-nav-back]');
    const navSecondaryCategory = document.querySelector('[data-nav-category]');
    const navPrimary = document.querySelectorAll('[data-nav-path]');

    navPrimary.forEach(el => {
        el.addEventListener('click', (e) => {
            const navSecondaryPath = el.dataset.navPath;
            navSecondaryCategory.textContent = el.querySelector('.nav-primary__text').textContent;
            navSecondary.classList.add('nav-secondary--open');

            navPrimary.forEach(elem => {
                elem.classList.remove('active');
            });
            navSecondarySections.forEach(elem => {
                elem.classList.remove('active');
            });
            nav.querySelector(`[data-nav-path="${navSecondaryPath}"]`).classList.add('active');
            nav.querySelector(`[data-nav-target="${navSecondaryPath}"]`).classList.add('active');

            navSecondary.classList.add('nav-secondary--open');
        //    navScroll.scrollContentEl.scrollTop = 0;
        });
    });

    navSecondaryBackward.addEventListener('click', e => {
        navSecondary.classList.remove('nav-secondary--open')
    });
}
navigation();


// tapSearch
document.addEventListener('click', tapSearch);
function tapSearch(event) {
    if (event.target.closest('[data-tap-search]')) {
        event.target.closest('[data-tap-search]').classList.toggle('active');
        document.querySelector('.mobile-search').classList.toggle('open');
        document.querySelector('.tapbar [data-nav-toggle]').classList.remove('active');
        document.querySelector('.nav').classList.remove('open');
        document.querySelector('body').classList.remove('overflow-hidden');
        document.querySelector('.nav-secondary').classList.remove('nav-secondary--open');
    }
    else if (event.target.closest('[data-search]')) {

    }
    else {
        document.querySelector('[data-tap-search]').classList.remove('active');
        document.querySelector('.mobile-search').classList.remove('open');
    }
}

// showmore

const showMore = document.querySelectorAll('[data-showmore-toggle]');
showMore.forEach(el => {
    el.addEventListener('click', (e) => {
        const self = e.currentTarget.closest('[data-showmore]');
        const control = self.querySelector('[data-showmore-toggle]');
        const content = self.querySelector('[data-showmore-content]');

        self.classList.toggle('open');

        if (self.classList.contains('open')) {
            control.setAttribute('aria-expanded', true);
            content.setAttribute('aria-hidden', false);
            content.style.maxHeight = content.scrollHeight + 'px';
        } else {
            control.setAttribute('aria-expanded', false);
            content.setAttribute('aria-hidden', true);
            content.style.maxHeight = null;
        }
        return false;
    });
});

// Filter
const filterToggle = document.querySelectorAll('[data-filter-toggle]');
if (filterToggle.length > 0) {
    filterToggle.forEach(el => {
        el.addEventListener('click', (e) => {
            document.querySelector('body').classList.toggle('filter-open');
            return false;
        });
    });
}

function productMedia() {
    const media = document.querySelector('[data-product-media]');

    if (media) {
        const thumbs = document.querySelectorAll('[data-slide-target]');

        const mediaGallery = new Swiper('[data-product-gallery]', {
            slidesPerView: 1,
            spaceBetween: 0,
            pagination: {
                el: '[data-product-pagination]',
                clickable: true,
            },
        });

        thumbs.forEach(el => {
            el.addEventListener('click', (e) => {
                const slideNext = e.currentTarget.dataset.slideTarget;
                console.log(slideNext);
                mediaGallery.slideTo(slideNext, 300);
            });
        });
    }
}
productMedia();

// Скролл шапки на странице товара
if(document.querySelector('[data-product-header]')) {
    document.addEventListener('scroll', function(event) {
        let productHeader = document.querySelector('[data-product-header]');
        if (window.pageYOffset > 280 ) {
            productHeader.classList.add('product-header--show');
        }
        else {
            productHeader.classList.remove('product-header--show');
        }
    });
}

// Поиск
function siteSearch() {
    const searchItems = document.querySelectorAll('[data-search]');
    if (searchItems.length > 0) {
        const searchFields = document.querySelectorAll('[data-search-input]');
        searchFields.forEach(el => {
            el.addEventListener('keyup', (event) => {

                const searchBlock = event.currentTarget.closest('[data-search]');
                let searchValue = event.currentTarget.value;
                if (searchValue.length > 2) {
                    searchBlock.classList.add('open');
                    console.log(searchValue);
                }
                else {
                    searchBlock.classList.remove('open');
                }

            })
        });

        document.addEventListener('click', function (event){
            if (!event.target.closest('[data-search]')) {
                searchItems.forEach(elem => {
                    elem.classList.remove('open');
                });
            }
        });
    }
}
siteSearch()

// Переключение каталога

document.addEventListener('click', productSwitcher);
function productSwitcher(event) {
    if (event.target.closest('[data-view-button]')) {
        const itemsContainer = document.querySelector('[data-items]');
        const items = itemsContainer.querySelectorAll('[data-item]');
        const buttonArray = document.querySelectorAll('[data-view-button]');
        buttonArray.forEach(elem => {
            elem.classList.remove('active');
        });
        let switchBtn = event.target.closest('[data-view-button]');
        let switchBtnValue = switchBtn.dataset.viewButton
        switchBtn.classList.add('active');

        if (switchBtnValue == 1) {
            items.forEach(elem => {
                elem.classList.add('item--expanded');
            });
        }
        else {
            items.forEach(elem => {
                elem.classList.remove('item--expanded');
            });
        }
    }
}

// Rating


document.addEventListener('click', raty);
function raty(event) {
    if (event.target.closest('[data-raty-item]')) {
        const rating = event.target.closest('[data-raty]');
        const ratingValues =  rating.querySelectorAll('[data-raty-item]');
        const ratingItem = event.target.closest('[data-raty-item]');
        const ratingValue = ratingItem.dataset.ratyItem;

        ratingValues.forEach(elem => {
            elem.classList.remove('checked');
        });
        ratingItem.classList.add('checked');
        rating.querySelector('[data-raty-input]').value = ratingValue;
        rating.querySelector('[data-raty-value]').innerHTML = ratingValue;
        console.log(ratingValue);

    }
}
