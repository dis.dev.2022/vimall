import webpack from "webpack-stream";
import TerserPlugin from "terser-webpack-plugin";

export const js = () => {
	return app.gulp.src(app.path.src.js, { sourcemaps: app.isDev })
		.pipe(app.plugins.plumber(
			app.plugins.notify.onError({
				title: "JS",
				message: "Error: <%= error.message %>"
			}))
		)
		.pipe(webpack({
			mode: "production",
			optimization: {
				minimize: false,
				minimizer: [new TerserPlugin()],
			},
			output: {
				filename: 'app.js',
				publicPath: '/',
			},
			performance: {
				hints: false
			}
		}))
		.pipe(app.gulp.dest(app.path.build.js))
		.pipe(app.plugins.browsersync.stream());
}
